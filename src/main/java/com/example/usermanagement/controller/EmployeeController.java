package com.example.usermanagement.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermanagement.model.*;
import com.example.usermanagement.repository.*;
import com.example.usermanagement.exception.*;



@RestController

@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeeRepo;
	
	
	//get all employee
	@GetMapping("/employees")
	public List<Employee> getAllEmployee(){
		return employeeRepo.findAll();
	}
	
	// create Employee rest Api
	@PostMapping("/employees/add")
	public Employee addEmployee(@RequestBody Employee employee) {
		return employeeRepo.save(employee);
		
	}
	
	//get employee by rest api
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
		Employee employee = employeeRepo.findById(id).orElseThrow( () -> new ResourceNotFoundException("Employee not exist with id: "+id));
		return ResponseEntity.ok(employee);
	}
	
	//update employee rest api
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployeeByiD(@PathVariable Long id, @RequestBody Employee employeedetails){
		Employee employee = employeeRepo.findById(id).orElseThrow( () -> new ResourceNotFoundException("Employee not exist with id: "+id));
		
		employee.setFirstname(employeedetails.getFirstname());
		employee.setLastname(employeedetails.getLastname());
		employee.setEmailid(employeedetails.getEmailid());
		
		Employee updatedemployee = employeeRepo.save(employee);
		return ResponseEntity.ok(updatedemployee);
	
	
	}
	//delete employee rest api
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable Long id){
		Employee employee = employeeRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not exist with id :" + id));
		
		employeeRepo.delete(employee);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}
