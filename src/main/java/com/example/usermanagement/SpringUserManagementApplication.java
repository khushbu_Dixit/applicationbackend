package com.example.usermanagement;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermanagement.model.*;
@SpringBootApplication
@RestController
@CrossOrigin(origins="*")
public class SpringUserManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringUserManagementApplication.class, args);
	}
	
	@GetMapping("/")
	public String login() {
		return "Authenticated successfully";
	}
	
	
	

}
