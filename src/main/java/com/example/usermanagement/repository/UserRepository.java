package com.example.usermanagement.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermanagement.model.*;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

	User findByUsername(String username);
}
