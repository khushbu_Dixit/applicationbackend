package com.example.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermanagement.model.*;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {

}
