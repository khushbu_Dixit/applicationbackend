package com.example.usermanagement.config;

import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		  http.cors();
		  http.csrf().disable();
		http.authorizeRequests().antMatchers(HttpMethod.OPTIONS,"/**").permitAll().antMatchers("/employees/**","/employees/add").permitAll().
		antMatchers("/login").fullyAuthenticated().and().httpBasic();
		}

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("arnetta").password("{noop}password").roles("USERS"); // for login purpose
	}



	//
	//@Bean
	//public BCryptPasswordEncoder encodePWD() {
		//return new BCryptPasswordEncoder();
	//}
}
